<?php

return [
    'informacion'=>'alpenombre2020@gmail.com',//correo de informacion
    'contacto'=>'alpenombre2020@gmail.com',//solicitud de correo de contacto
    'responder'=>'empresa@alpe.es',//direccion a quien responder
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
];
